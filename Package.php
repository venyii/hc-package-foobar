<?php

namespace HCPackage\FooBar;

use Venyii\HipChatCommander\Package\AbstractPackage;
use Venyii\HipChatCommander\Api\Response;

class Package extends AbstractPackage
{
    /**
     * {@inheritdoc}
     */
    public function configure()
    {
        $this
            ->setName('foobar')
            ->addCommand('help')
            ->addCommand('foo', null, [], true)
        ;
    }

    /**
     * @return Response
     */
    public function fooCmd()
    {
        return Response::create('bar');
    }
}

